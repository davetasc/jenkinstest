//speclogin.js


describe('Protractor Login Screen', function() {
it('Login into dev environment', function() {

browser.get(browser.params.baseUrl.test);

protractor.page.loginToPage(browser.params.login.email, browser.params.login.password);


element(by.linkText('Clients')).click();


element(by.xpath('//div[@id="content"]/div/div/div/app-card/div/div/div/button')).click();

element(by.name('name')).sendKeys('Automation Test');
element(by.name('ein')).sendKeys('44-4444444');
element(by.name('filingStatus')).sendKeys('C Corp');

browser.sleep(2000);

element(by.xpath('//div[@id="content"]/div/div/div/form/fieldset/app-address-list/app-card/div/header/div/ul/li/button')).click();

element(by.name('addressLine1')).sendKeys('Address Line 1 Test');
element(by.name('addressLine2')).sendKeys('Address Line 2 Test');
element(by.name('city')).sendKeys('Madison');
element(by.name('country')).sendKeys('United States');
element(by.name('state')).sendKeys('WI');
element(by.name('zip')).sendKeys('53703');

browser.sleep(2000);

element(by.buttonText('Ok')).click();

browser.sleep(2000);

element(by.xpath('//div[@id="content"]/div/div/div/form/fieldset/app-contact-list/app-card/div/header/div/ul/li/button')).click();

element.all(by.name('name')).last().sendKeys('Contact Name');
element(by.name('email')).sendKeys('email@address.com');
element(by.name('phone')).sendKeys('608-655-6555');
element(by.name('ext')).sendKeys('654');
element(by.name('title')).sendKeys('Contract');

browser.sleep(2000);

element(by.buttonText('Ok')).click();

browser.sleep(2000);

element(by.xpath('//div[@id="content"]/div/div/div/form/fieldset/app-payroll-list/app-card/div/header/div/ul/li/button')).click();

element.all(by.name('name')).last().sendKeys('Entered Payroll Name');
element(by.name('frequency')).sendKeys('Monthly');

browser.sleep(2000);

element(by.buttonText('Ok')).click();

browser.sleep(2000);

element(by.xpath('//div[@id="content"]/div/div/div/form/fieldset/app-account-list/app-card/div/header/div/ul/li/button')).click();

element.all(by.name('name')).last().sendKeys('Account Name');
element(by.name('accountType')).sendKeys('Checking');
element(by.name('bankName')).sendKeys('Chase');
element(by.name('accountNumber')).sendKeys('1234');
element(by.name('matchAccountNumber')).sendKeys('1234');
element(by.name('routingNumber')).sendKeys('456783334');

browser.sleep(2000);

element(by.buttonText('Ok')).click();

browser.sleep(2000);

element(by.buttonText('Save')).click();

browser.sleep(6000);

});
});
